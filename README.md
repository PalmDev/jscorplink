**HTML**

Вставлять в последний блок на сайте (не в конец body)

    <a href="https://jscorp.ru/red.html" class="jscorp-link" target="_blank" style="right: 40px; background-color: #DD1733;">
         <img src="jscorp-link_logo.png" alt="JS">  
    </a>
![enter image description here](https://sun1-17.userapi.com/zAcwUDD--UUXdNi4iRlN15wdLDEJuVQAJLOIDw/oGqNZ3CQsPQ.jpg)

**CSS**

    .jscorp-link {  
      width: 70px;  
      height: 60px;  
      cursor: pointer;  
      transition: all 0.3s ease;  
      display: block;  
      position: absolute;  
      bottom: 0px;  
      padding-top: 17px; 
    }  
    .jscorp-link img {  
      width: 75%;  
      display: block;  
      margin: auto; 
    }  
    .jscorp-link:hover {  
      height: 90px; 
	}

**IMG**

jscorp-link_logo.png кидать в корень сайта, где находятся HTML.




